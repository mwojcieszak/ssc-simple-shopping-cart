<?php

namespace RXBundle\Controller;

use RXBundle\Entity\Cart;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use RXBundle\Entity\CartProduct;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $categories = $this->getDoctrine()->getRepository('RXBundle:Category')->getCategories();

        return $this->render('RXBundle:Default:index.html.twig', ['categories' => $categories]);
    }

    /**
     * @Route("/product-add/{id}", name="product-add")
     */
    public function addAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $cart = $this->get('rx.cart')->getInstance($request->getSession()->getId());

        $product = $this->getDoctrine()->getRepository('RXBundle:Product')->find($id);

        if (!$product) {
            throw new BadRequestHttpException();
        }

        $cp = new CartProduct();
        $cp->setProduct($product);
        $cp->setCart($cart);

        $em->persist($cp);
        $em->flush();

        return $this->redirect($this->generateUrl('homepage'));
    }
}
