<?php

namespace RXBundle\Controller;

use RXBundle\Entity\Cart;
use RXBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use RXBundle\Form\ProductListType;
use Symfony\Component\HttpFoundation\Request;

class CartController extends Controller
{
    /**
     * @Route("/cart", name="cart-index")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $sessionId = $request->getSession()->getId();

        $cart = $em->getRepository('RXBundle:Cart')->findCartBySession($sessionId);

        if (empty($cart)) {
            $cart = new Cart($sessionId);
        }

        $form = $this->createForm(new ProductListType($cart));

        $form->handleRequest($request);

        $summary = $this->get('rx.cart')->getSummary($cart->getProducts());

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($form->getData()['product'] as $product) {
                if ($cart->getProducts()->contains($product)) {
                    $cart->getProducts()->removeElement($product);
                }
            }

            $em->persist($cart);

            $em->flush();

            return $this->redirect($this->generateUrl('homepage'));
        }

        return $this->render('RXBundle:Cart:index.html.twig', ['form' => $form->createView(), 'summary' => $summary]);
    }
}
