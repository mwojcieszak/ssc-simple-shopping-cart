<?php

namespace MT\APIBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use RXBundle\Entity\Product;
use RXBundle\Entity\Category;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritdoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $categories = [
            ['name' => 'Produkt spożywczy'],
            ['name' => 'Książka'],
        ];

        $books = [
            ['name' => 'Stephen King -  Dallas 63', 'price' => '42.00', 'vat' => 8],
            ['name' => 'Dan Brown - Zaginiony Symbol', 'price' => '39.00', 'vat' => 23],
        ];

        $products = [
            ['name' => 'Baton Mars', 'price' => '3', 'vat' => 23],
            ['name' => 'Coca-cola', 'price' => '2', 'vat' => 23],
        ];

        foreach ($categories as $index => $element) {
            $category = new Category();
            $category->setName($element['name']);

            $manager->persist($category);

            $this->addReference('category-'.$index, $category);
        }

        foreach ($products as $element) {
            $product = new Product();
            $product->setName($element['name']);
            $product->setPrice($element['price']);
            $product->setVat($element['vat']);

            $product->setCategory($this->getReference('category-0'));
            $manager->persist($product);
        }

        foreach ($books as $element) {
            $product = new Product();
            $product->setName($element['name']);
            $product->setPrice($element['price']);
            $product->setVat($element['vat']);

            $product->setCategory($this->getReference('category-1'));
            $manager->persist($product);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 50;
    }
}
