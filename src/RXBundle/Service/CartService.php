<?php

namespace RXBundle\Service;

use Doctrine\Bundle\DoctrineBundle\Registry;
use RXBundle\Entity\Cart;

class CartService
{
    private $doctrine;

    public function __construct(Registry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * @param $sessionId
     *
     * @return Cart
     */
    public function getInstance($sessionId)
    {
        $em = $this->doctrine->getManager();
        $cart = $em->getRepository('RXBundle:Cart')->findCartBySession($sessionId);

        if (!$cart) {
            $cart = new Cart();
            $cart->setSessionId($sessionId);

            $em->persist($cart);
            $em->flush();
        }

        return $cart;
    }

    /**
     * @param $products
     *
     * @return mixed
     */
    public function getSummary($products)
    {
        $summary = [];

        foreach ($products as $product) {
            if (isset($summary[$product->getProduct()->getVat()])) {
                $summary[$product->getProduct()->getVat()] = $summary[$product->getProduct()->getVat()] + $product->getProduct()->getPriceWithTax();
            } else {
                $summary[$product->getProduct()->getVat()] = $product->getProduct()->getPriceWithTax();
            }
        }

        return $summary;
    }
}
