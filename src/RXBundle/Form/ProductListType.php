<?php

namespace RXBundle\Form;

use RXBundle\Entity\Cart;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ProductListType extends AbstractType
{
    private $cart;

    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('product', 'entity', [
                'class' => 'RXBundle:CartProduct',
                'choices' => $this->cart->getProducts(),
                'expanded' => true,
                'multiple' => true,
                'label' => false,
            ])
            ->add('submit', 'submit', ['label' => 'Usuń zaznaczone']);
    }
}
