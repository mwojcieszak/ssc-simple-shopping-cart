<?php

namespace RXBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Product.
 *
 * @ORM\Table(name="products")
 * @ORM\Entity(repositoryClass="RXBundle\Repository\ProductRepository")
 */
class Product
{
    const CURRENCY = 'zł';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="float")
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="vat", type="integer")
     */
    private $vat;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="products")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="CartProduct", mappedBy="product", cascade={"remove"} )
     */
    private $products;

    /**
     * @JMS\Accessor(getter="getPriceWithTax")
     */
    private $priceWithTax;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price.
     *
     * @param float $price
     *
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set vat.
     *
     * @param int $vat
     *
     * @return Product
     */
    public function setVat($vat)
    {
        $this->vat = $vat;

        return $this;
    }

    /**
     * Get vat.
     *
     * @return int
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * Set category.
     *
     * @param \RXBundle\Entity\Category $category
     *
     * @return Product
     */
    public function setCategory(\RXBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category.
     *
     * @return \RXBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    public function getPriceWithTax()
    {
        return $this->price + ($this->price * $this->vat / 100);
    }

    public function __toString()
    {
        return "{$this->getName()}";
    }

    /**
     * Add product.
     *
     * @param \RXBundle\Entity\CartProduct $product
     *
     * @return Product
     */
    public function addProduct(\RXBundle\Entity\CartProduct $product)
    {
        $this->products[] = $product;

        return $this;
    }

    /**
     * Remove product.
     *
     * @param \RXBundle\Entity\CartProduct $product
     */
    public function removeProduct(\RXBundle\Entity\CartProduct $product)
    {
        $this->products->removeElement($product);
    }

    /**
     * Get products.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
        return $this->products;
    }
}
