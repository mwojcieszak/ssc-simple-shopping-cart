SSC -Simple Shopping Cart
========================

Project based on Symfony 2.7


Installation
------------

### Add your user to www-data group

### Make sure APC is enabled

### Create database and define config/parameters.yml

### Build databases
    php app/console doctrine:schema:update --force

### Load fixtures
    php app/console doctrine:fixtures:load

### Run composer to get all dependencies
    composer install

### Install bootstrap
    bower install bootstrap

### Run server
    php app/console server:run --env="prod"